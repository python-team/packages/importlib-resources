.coveragerc
.editorconfig
.gitattributes
.gitignore
.pre-commit-config.yaml
.readthedocs.yaml
LICENSE
NEWS.rst
README.rst
SECURITY.md
codecov.yml
mypy.ini
pyproject.toml
pytest.ini
ruff.toml
towncrier.toml
tox.ini
.github/FUNDING.yml
.github/dependabot.yml
.github/workflows/main.yml
docs/api.rst
docs/conf.py
docs/history.rst
docs/index.rst
docs/migration.rst
docs/using.rst
importlib_resources/__init__.py
importlib_resources/_adapters.py
importlib_resources/_common.py
importlib_resources/_functional.py
importlib_resources/_itertools.py
importlib_resources/abc.py
importlib_resources/py.typed
importlib_resources/readers.py
importlib_resources/simple.py
importlib_resources.egg-info/PKG-INFO
importlib_resources.egg-info/SOURCES.txt
importlib_resources.egg-info/dependency_links.txt
importlib_resources.egg-info/requires.txt
importlib_resources.egg-info/top_level.txt
importlib_resources/compat/__init__.py
importlib_resources/compat/py39.py
importlib_resources/future/__init__.py
importlib_resources/future/adapters.py
importlib_resources/tests/__init__.py
importlib_resources/tests/_path.py
importlib_resources/tests/test_compatibilty_files.py
importlib_resources/tests/test_contents.py
importlib_resources/tests/test_custom.py
importlib_resources/tests/test_files.py
importlib_resources/tests/test_functional.py
importlib_resources/tests/test_open.py
importlib_resources/tests/test_path.py
importlib_resources/tests/test_read.py
importlib_resources/tests/test_reader.py
importlib_resources/tests/test_resource.py
importlib_resources/tests/test_util.py
importlib_resources/tests/util.py
importlib_resources/tests/zip.py
importlib_resources/tests/compat/__init__.py
importlib_resources/tests/compat/py312.py
importlib_resources/tests/compat/py39.py